
- Provisioning regional Persistent Disks 

```bash
cloud_user_p_e27bd1@cloudshell:~$ cat rpvc.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: rpvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 200Gi
  storageClassName: rdisk


cloud_user_p_e27bd1@cloudshell:~$ cat regiondisk.yaml
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: rdisk
provisioner: kubernetes.io/gce-pd
parameters:
  type: pd-standard
  replication-type: regional-pd
  zones: asia-southeast1-a, asia-southeast1-c, asia-southeast1-b



cloud_user_p_e27bd1@cloudshell:~$ kubectl describe pv pvc-6ffb05dd-ddc4-11e9-b95e-42010a94004e
Name:              pvc-6ffb05dd-ddc4-11e9-b95e-42010a94004e
Labels:            failure-domain.beta.kubernetes.io/region=asia-southeast1
                   failure-domain.beta.kubernetes.io/zone=asia-southeast1-c__asia-southeast1-a
Annotations:       kubernetes.io/createdby: gce-pd-dynamic-provisioner
                   pv.kubernetes.io/bound-by-controller: yes
                   pv.kubernetes.io/provisioned-by: kubernetes.io/gce-pd
Finalizers:        [kubernetes.io/pv-protection]
StorageClass:      rdisk
Status:            Bound
Claim:             default/rpvc
Reclaim Policy:    Delete
Access Modes:      RWO
VolumeMode:        Filesystem
Capacity:          200Gi
Node Affinity:
  Required Terms:
    Term 0:        failure-domain.beta.kubernetes.io/zone in [asia-southeast1-c, asia-southeast1-a]
                   failure-domain.beta.kubernetes.io/region in [asia-southeast1]
Message:
Source:
    Type:       GCEPersistentDisk (a Persistent Disk resource in Google Compute Engine)
    PDName:     gke-standard-cluster-1-pvc-6ffb05dd-ddc4-11e9-b95e-42010a94004e
    FSType:     ext4
    Partition:  0
    ReadOnly:   false
Events:         <none>

cloud_user_p_e27bd1@cloudshell:~$ gcloud compute disks list --project playground-s-11-b77010
NAME                                                             LOCATION           LOCATION_SCOPE  SIZE_GB  TYPE         STATUS
gke-standard-cluster-1-pvc-6ffb05dd-ddc4-11e9-b95e-42010a94004e  asia-southeast1    region          200      pd-standard  READY
gke-standard-cluster-1-default-pool-04ad431c-dhmb                asia-southeast1-c  zone            100      pd-standard  READY
gke-standard-cluster-1-default-pool-04ad431c-mnmz                asia-southeast1-c  zone            100      pd-standard  READY
gke-standard-cluster-1-default-pool-c5ab3907-f1fp                asia-southeast1-b  zone            100      pd-standard  READY
gke-standard-cluster-1-default-pool-c5ab3907-txxh                asia-southeast1-b  zone            100      pd-standard  READY
gke-standard-cluster-1-pvc-1a28cc20-ddb4-11e9-bd6e-42010a9400bb  asia-southeast1-b  zone            1        pd-standard  READY
gke-standard-cluster-1-default-pool-f20a1ce6-1rk4                asia-southeast1-a  zone            100      pd-standard  READY
gke-standard-cluster-1-default-pool-f20a1ce6-ktsr                asia-southeast1-a  zone            100      pd-standard  READY
cloud_user_p_e27bd1@cloudshell:~$ kubectl get pv
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM          STORAGECLASS   REASON   AGE
pvc-1a28cc20-ddb4-11e9-bd6e-42010a9400bb   1Gi        RWO            Delete           Bound    default/pvc    standard                129m
pvc-6ffb05dd-ddc4-11e9-b95e-42010a94004e   200Gi      RWO            Delete           Bound    default/rpvc   rdisk                   12m
cloud_user_p_e27bd1@cloudshell:~$

```



- New PV with regional disk (storage class)

```bash

  labels:
    failure-domain.beta.kubernetes.io/region: asia-southeast1
    failure-domain.beta.kubernetes.io/zone: asia-southeast1-c__asia-southeast1-a

 nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: failure-domain.beta.kubernetes.io/zone
          operator: In
          values:
          - asia-southeast1-c
          - asia-southeast1-a
        - key: failure-domain.beta.kubernetes.io/region
          operator: In
          values:
          - asia-southeast1
```

- Old PV without regional disk 

```bash
  labels:
    failure-domain.beta.kubernetes.io/region: asia-southeast1
    failure-domain.beta.kubernetes.io/zone: asia-southeast1-b
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: failure-domain.beta.kubernetes.io/zone
          operator: In
          values:
          - asia-southeast1-b
        - key: failure-domain.beta.kubernetes.io/region
          operator: In
          values:
          - asia-southeast1
```