Need to create or update StorageClass with volumeBindingMode as `WaitForFirstConsumer`

```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: standard
provisioner: kubernetes.io/gce-pd
parameters:
  type: pd-standard
volumeBindingMode: WaitForFirstConsumer
```

```bash
cloud_user_p_e27bd1@cloudshell:~$ kubectl get pv,pvc
NAME                                                        CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM             STORAGECLASS   REASON   AGE
persistentvolume/pvc-0c8a2539-ddcd-11e9-a967-42010a940081   200Gi      RWO            Delete           Bound    default/waitpvc   waitdisk                28s


NAME                            STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
persistentvolumeclaim/waitpvc   Bound    pvc-0c8a2539-ddcd-11e9-a967-42010a940081   200Gi      RWO            waitdisk       33s

cloud_user_p_e27bd1@cloudshell:~$ kubectl get po appz-7498d9965b-l9k9t
NAME                    READY   STATUS    RESTARTS   AGE
appz-7498d9965b-l9k9t   1/1     Running   0          39s

cloud_user_p_e27bd1@cloudshell:~$ gcloud compute disks list --project playground-s-11-b77010
NAME                                                             LOCATION           LOCATION_SCOPE  SIZE_GB  TYPE         STATUS
gke-standard-cluster-1-default-pool-04ad431c-dhmb                asia-southeast1-c  zone            100      pd-standard  READY
gke-standard-cluster-1-default-pool-04ad431c-mnmz                asia-southeast1-c  zone            100      pd-standard  READY
gke-standard-cluster-1-default-pool-c5ab3907-f1fp                asia-southeast1-b  zone            100      pd-standard  READY
gke-standard-cluster-1-default-pool-c5ab3907-txxh                asia-southeast1-b  zone            100      pd-standard  READY
gke-standard-cluster-1-pvc-1a28cc20-ddb4-11e9-bd6e-42010a9400bb  asia-southeast1-b  zone            1        pd-standard  READY
gke-standard-cluster-1-default-pool-f20a1ce6-1rk4                asia-southeast1-a  zone            100      pd-standard  READY
gke-standard-cluster-1-default-pool-f20a1ce6-ktsr                asia-southeast1-a  zone            100      pd-standard  READY
gke-standard-cluster-1-pvc-0c8a2539-ddcd-11e9-a967-42010a940081  asia-southeast1-a  zone            200      pd-standard  READY

cloud_user_p_e27bd1@cloudshell:~$ kubectl get node  --show-labels | grep -i ktsr
gke-standard-cluster-1-default-pool-f20a1ce6-ktsr   Ready    <none>   3h40m   v1.13.7-gke.8   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/fluentd-ds-ready=true,beta.kubernetes.io/instance-type=n1-standard-1,beta.kubernetes.io/os=linux,cloud.google.com/gke-nodepool=default-pool,cloud.google.com/gke-os-distribution=cos,failure-domain.beta.kubernetes.io/region=asia-southeast1,failure-domain.beta.kubernetes.io/zone=asia-southeast1-a,kubernetes.io/hostname=gke-standard-cluster-1-default-pool-f20a1ce6-ktsr

cloud_user_p_e27bd1@cloudshell:~$ gcloud compute disks list --project playground-s-11-b77010 | grep -i pvc-0c8a2539-ddcd-11e9-a967-42010a940081
NAME                                                             LOCATION           LOCATION_SCOPE  SIZE_GB  TYPE         STATUS
gke-standard-cluster-1-pvc-0c8a2539-ddcd-11e9-a967-42010a940081  asia-southeast1-a  zone            200      pd-standard  READY



cloud_user_p_e27bd1@cloudshell:~$ cat waitSC.yaml
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: waitdisk
provisioner: kubernetes.io/gce-pd
parameters:
  type: pd-standard
volumeBindingMode: WaitForFirstConsumer
#  replication-type: regional-pd
#  zones: asia-southeast1-a, asia-southeast1-c, asia-southeast1-b

cloud_user_p_e27bd1@cloudshell:~$ cat waitpvc.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: waitpvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 200Gi
  storageClassName: waitdisk

cloud_user_p_e27bd1@cloudshell:~$ cat app-z.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    run: app
    release: z
  name: appz
spec:
  strategy:
    type: Recreate
  replicas: 1
  selector:
    matchLabels:
      run: nginx
  template:
    metadata:
      labels:
        run: nginx
    spec:
      nodeSelector:
              #        "kubernetes.io/hostname": gke-standard-cluster-1-default-pool-04ad431c-dhmb
        "kubernetes.io/hostname": gke-standard-cluster-1-default-pool-f20a1ce6-ktsr
      containers:
      - image: nginx
        name: nginx
      volumes:
        - name: datadir
          persistentVolumeClaim:
            claimName: waitpvc
cloud_user_p_e27bd1@cloudshell:~$

```